//
//  ContentView.swift
//  test
//
//  Created by Ebrahim Joy on 19/6/21.
//

import SwiftUI

struct ContentView: View {
    @State var card:String = "card5"
    @State var cpuCard:String = "card9"
    @State var cardResult:Int = 0
    @State var cpuResult:Int = 0

    var body: some View {
        
    
        ZStack{
       Image("background").ignoresSafeArea()
            
            VStack{
               
                Image("logo").padding(.vertical)
        
              
                HStack{
                    Spacer()
                    Image(card)
                    
                    Spacer()
                    Image(cpuCard)
                    Spacer()
                }
                
              
              
                
                Button(action: {
                    
                    let playerRandom = Int.random(in: 2...14)
                    let cpuRandom = Int.random(in: 2...14)
                    
                    card = "card" + String(playerRandom);
                    cpuCard = "card" + String(cpuRandom);
                    
                    if(playerRandom>cpuRandom){
                        cardResult = cardResult+1
                    }else{
                        cpuResult = cpuResult+1
                    }
                   
                   
                
                    
                 
                  
                
                    
                }, label: {
                    Image("dealbutton").padding(32)
                })
                
              

                
                HStack{
                    Spacer()
                    VStack{
                        Text("Player")
                            .font(.title)
                            .padding(.all)
                            .foregroundColor(.black)
                        Text(String(cardResult))
                            .font(.title3)
                            .foregroundColor(.black)
                       
                        
                    
                      
                    }
                    Spacer()
                   
                    VStack{
                        Text("Computer")
                            .font(.title)
                            .padding(.all)
                            .foregroundColor(.black)
                        
                        Text(String(cpuResult))
                            .font(.title3)
                            .foregroundColor(.black)
                
                    }
                    Spacer()
                
                }
            
              
            
            }
            
            
            
        }
        
       
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
