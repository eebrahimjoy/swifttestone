//
//  testApp.swift
//  test
//
//  Created by Ebrahim Joy on 19/6/21.
//

import SwiftUI

@main
struct testApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
